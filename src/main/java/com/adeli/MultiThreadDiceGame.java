package com.adeli;

import java.util.Scanner;

/**
 * Dice Game between human & computer
 *
 * @author Mohsen Adelimoghaddam
 * @version 1.0.0
 */
public class MultiThreadDiceGame {
    static int round;
    static int wins;
    static int losses;
    static int draw;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter 'Put Dice Game' rounds: ");
        round = scan.nextInt();
        System.out.println("Human and Computer are putting dice...");

        PutDice rc = new PutDice("computer", round);
        Thread tc = new Thread(rc);
        PutDice rh = new PutDice("human", round);
        Thread th = new Thread(rh);

        tc.start();
        th.start();

        try {
            tc.join();
            th.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // for determine the results
        for (int i = 0; i < round; i++) {
            if (rh.getResult().get(i) > rc.getResult().get(i))
                wins++;
            else if (rh.getResult().get(i) < rc.getResult().get(i))
                losses++;
            else
                draw++;
        }
        System.out.printf("Human result is => Wins: %d, Losses: %d, Draw: %d",
                wins, losses, draw);
    }
}
