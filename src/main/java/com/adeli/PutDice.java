package com.adeli;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Simulate putting dice in random time
 * for a specific number by human or computer.
 */
public class PutDice implements Runnable {
    private String player;
    private int round;
    private Random rand;
    private List<Integer> result = new ArrayList<>();

    public PutDice(String player, int round) {
        this.player = player.toLowerCase();
        this.round = round;
        rand = new Random();
    }

    @Override
    public void run() {
        if ("computer".equals(player)) {
            dice(result, "Computer results: ");
        } else if ("human".equals(player)) {
            dice(result, "Human    results: ");
        }
        System.out.println();
    }

    /**
     *
     * @param list
     * @param message
     */
    private void dice(List<Integer> list, String message) {
        for (int i = 0; i < round; i++) {
            list.add(rand.nextInt(6) + 1);
            try {
                Thread.sleep(rand.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // lock the console to print all result
        synchronized (System.out) {
            System.out.print(message);
            for (Integer i : result) {
                System.out.print(i + " ");
            }
        }
    }

    public List<Integer> getResult() {
        return result;
    }
}
